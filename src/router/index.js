import Vue from 'vue'
import Router from 'vue-router'
// import Hello from '@/components/Hello'
import CitizenList from '@/components/citizen_list'
import CitizenAdd from '@/components/citizen_add'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'CitizenList',
      component: CitizenList
    },
    {
      path: '/citizen_add',
      name: 'CitizenAdd',
      component: CitizenAdd
    }
  ]
})
